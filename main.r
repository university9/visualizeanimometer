#-----------------------------------------
# Curso: Bases de datos II grupo 2
# Tarea: Proyecto R
# Estudiantes: Jorge Arturo Vásquez Rojas
#              David Salazar Vásquez
# II Semestre 2019
#-----------------------------------------
library("ggplot2")
setwd('D:\\')
getwd()
#[1] "/Users/diego"
read.table("animometro2.csv", header=TRUE)
misdatos <- read.table("animometro2.csv", header = TRUE, sep=",")


misdatos$week <- as.integer(strftime(as.Date(misdatos$date, format = "%m/%d/%Y %H:%M"), format = "%V"))-29
misdatos$hora <- as.integer(format(strptime(misdatos$date,"%m/%d/%Y %H:%M"),'%H'))


#Graficos propuestos:

#Grafico 1 Conteo de estados por semana del semestre
p <- ggplot(data = misdatos, 
            mapping =  aes(x =  factor(week), fill = factor(happy, labels = c("Triste","Feliz") ),  label = "count")) 

p + geom_bar(position = "dodge", stat = "count") +  labs (
  title = "Conteo de estados por semana del semestre",
  caption = "II semestre 2019",
  fill = "estado",
  x = "semanas",
  y = "conteo"
) 
#Conclusión: Se puede apreciar que en la semana 10 y 11 hubo muchos participantes que denotaron un estado de tristeza,
#lo cual coincide con la semana de entrega de resultados de las evaluaciones hechas en la semana 8, por otro lado 
#la semana 12 y 14 presentaron una mayor cantidad de personas felices lo que podrian considerarse como semanas 
#en las cuales el nivel de estres se redujo bastante


p <- ggplot(data = misdatos, 
            mapping =  aes(x =  factor(hora), fill = factor(happy, labels = c("Triste","Feliz") ),  label = "count")) 

p + geom_bar(position = "fill", stat = "count") +  labs (
  title = "Conteo de estados por hora del dia",
  caption = "II semestre 2019",
  fill = "estado",
  x = "hora del dia",
  y = "conteo"
) 
#Conclusión: Las horas mas tristes del dia es a la 1pm, 4pm y 6pm. Mientras que las mas felices son a las 7am, 10am y 8pm


# 
#Resume los datos por fecha
misdatos$DateTime <- as.Date(misdatos$date, format = "%m/%d/%Y %H:%M")
df1 <- aggregate(list(count = misdatos$happy) , by=list(date = misdatos$DateTime), length)
df2 <-aggregate(list(happy = misdatos$happy) , by=list(date = misdatos$DateTime), sum)
datos <- merge(x=df1, y=df2,by.x = "date",by.y = "date")
datos$porcentajeFelicidad <- datos$happy/datos$count
datos$sad <- -datos$happy + datos$count
datos$ID <- seq.int(nrow(datos))


ct <- cor.test(datos$ID,datos$porcentajeFelicidad)
ct$estimate
#Conclusion: 
#Si el coeficiente de correlacion se encutra entre 0.2 y -0.2 entonces se considera que no existe
#relación entre las variables, por lo tanto la relación "Cantidad de personas felices por dia evaluado"
#no existe



fit = lm(happy ~ ID, datos)
summary(fit)
predict(fit)



ggplot(datos, aes(ID,happy )) + geom_point() + geom_smooth(method="lm") + labs (
  title = "Cantidad de participantes felices por dia evaluado",
  caption = "II semestre 2019",
  x = "dia evaluado",
  y = "cantidad de personas felices"
) 
#Conclusion: 
#Como en el ejercicio anterior se optuvo que no existe relación entre las variables, entonces los datos optenidos
#se podrian considerase que los calculos son erroneos.